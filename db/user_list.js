// user_list.js
 
const mysql = require('mysql');
 
const pool = mysql.createPool({
    connectionLimit: process.env.CONNECTION_LIMIT,    // the number of connections node.js will hold open to our database
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.MYSQL_DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
 
});
 
let user_list = {};
 
user_list.getAllUsers = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM userlist ',  (error, users)=>{
            if(error){
                return reject(error);
            }
            return resolve(users);
        });
    });
};

user_list.getUserByName = (query) =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM userlist WHERE name=?', [query.name],  (error, users)=>{
            if(error){
                return reject(error);
            }
            console.log('all search: ' + query.name);
            return resolve(users);
        });
    });
};

user_list.getUserBySurname = (query) =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM userlist WHERE surname=?', [query.surname],  (error, users)=>{
            if(error){
                return reject(error);
            }
            console.log('all search: ' + query.surname);
            return resolve(users);
        });
    });
};
 
user_list.getUserByEmail = (email) =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM userlist WHERE email= ?', [email], (error, users)=>{
            if(error){
                return reject(error);
            }
            return resolve(users);
        });
    });
};
 
user_list.insertUser = (email, name, surname, birthDate) =>{
    return new Promise((resolve, reject)=>{
        pool.query('INSERT INTO userlist (email, name, surname, birthDate) VALUES (?, ?, ?, ?)', [email, name, surname, birthDate], (error, result)=>{
            if(error){
                return reject(error);
            }
             
              return resolve(result.email);
        });
    });
};
 
 
user_list.updateUser = (name, surname, birthDate, email) =>{
    return new Promise((resolve, reject)=>{
        pool.query('UPDATE userlist SET name = ?, surname= ?, birthDate= ? WHERE email = ?', [name, surname, birthDate, email], (error)=>{
            if(error){
                return reject(error);
            }
             
              return resolve();
        });
    });
};
 
user_list.deleteUser = (email) =>{
    return new Promise((resolve, reject)=>{
        pool.query('DELETE FROM userlist WHERE email= ?', [email], (error)=>{
            if(error){
                return reject(error);
            }
              return resolve();
         
        });
    });
};
  
module.exports = user_list;
