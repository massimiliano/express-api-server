// cats.js
 
const mysql = require('mysql');
 
const pool = mysql.createPool({
    connectionLimit: process.env.CONNECTION_LIMIT,    // the number of connections node.js will hold open to our database
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.MYSQL_DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT 
});
 
let cats = {};
 
cats.getAllCats = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM cats ',  (error, cats)=>{
            if(error){
                return reject(error);
            }
            return resolve(cats);
        });
    });
};
 
cats.getOneCat = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM cats WHERE id= ?', [id], (error, cats)=>{
            if(error){
                return reject(error);
            }
            return resolve(cats);
        });
    });
};
 
cats.insertCat = (name, imageAddress) =>{
    return new Promise((resolve, reject)=>{
        pool.query('INSERT INTO cats (name, imageAddress) VALUES (?, ?)', [name, imageAddress], (error, result)=>{
            if(error){
                return reject(error);
            }
             
              return resolve(result.insertId);
        });
    });
};
 
 
cats.updateCat = (name, imageAddress, id) =>{
    return new Promise((resolve, reject)=>{
        pool.query('UPDATE cats SET name = ?, imageAddress= ? WHERE id = ?', [name, imageAddress, id], (error)=>{
            if(error){
                return reject(error);
            }
             
              return resolve();
        });
    });
};
 
cats.deleteCat = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query('DELETE FROM cats WHERE id= ?', [id], (error)=>{
            if(error){
                return reject(error);
            }
              return resolve();
         
        });
    });
};
  
module.exports = cats;
