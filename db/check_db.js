// check_db.js
const cats_population = require('../example/cats_population');
const mysql = require('mysql');

 
const pool = mysql.createPool({
    connectionLimit: process.env.CONNECTION_LIMIT,    // the number of connections node.js will hold open to our database
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.MYSQL_DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
 
});
 
let check_db = {};
 
check_db.getApiConnection = async () =>{
    return new Promise((resolve, reject)=>{
        pool.getConnection((error, connection)=>{
            if(error){
                if(error.code==='ER_BAD_DB_ERROR'){
                    return resolve(false);
                }
                else{
                    return reject(error);
                }
            }
            return resolve(true);
        });
    });
};

check_db.getUserlistConnection = async () =>{
    let getApiConnection = await check_db.getApiConnection();
    return new Promise((resolve, reject)=>{
        if (getApiConnection){
        pool.query('SHOW TABLES LIKE "userlist"',  (error, tables)=>{
            if(error){
                return reject(error);
            }
            if (tables.length < 1)return resolve(false);
            else return resolve(true);
        });
    }
    else return resolve(false);
});
};

check_db.getuserlistPopulation = async () =>{
    let catsConnection= await check_db.getUserlistConnection();
    return new Promise((resolve, reject)=>{
        if (catsConnection){
        pool.query('SELECT * FROM userlist ',  (error, cats)=>{
            if(error){
                return reject(error);
            }
            if (cats.length<1)return resolve(false);
            else return resolve(true);
        });
    }
    else return resolve(false);
});
};

check_db.getCatsConnection = async () =>{
    let getApiConnection = await check_db.getApiConnection();
    return new Promise((resolve, reject)=>{
        if (getApiConnection){
        pool.query('SHOW TABLES LIKE "cats"',  (error, tables)=>{
            if(error){
                return reject(error);
            }
            if (tables.length < 1)return resolve(false);
            else return resolve(true);
        });
    }
    else return resolve(false);
});
};

check_db.getCatsPopulation = async () =>{
    let catsConnection= await check_db.getCatsConnection();
    return new Promise((resolve, reject)=>{
        if (catsConnection){
        pool.query('SELECT * FROM cats ',  (error, cats)=>{
            if(error){
                return reject(error);
            }
            if (cats.length<1)return resolve(false);
            else return resolve(true);
        });
    }
    else return resolve(false);
});
};

check_db.getCatsExamplePopulation = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM cats ',  (error, cats)=>{
            if(error){
                return reject(error);
            }
            if (JSON.stringify(cats)!==JSON.stringify(cats_population))return resolve(false);
            else return resolve(true);
        });
    });
};
  
module.exports = check_db;
