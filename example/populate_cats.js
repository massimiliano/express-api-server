// populate_cats.js
 
const mysql = require('mysql');
 
const pool = mysql.createPool({
    connectionLimit: process.env.CONNECTION_LIMIT,    // the number of connections node.js will hold open to our database
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.MYSQL_DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
});
 
let populate_cats = {};

populate_cats.insertExampleCats = (cats_population) =>{
    for (const key in cats_population) {  
        let name=cats_population[key].name;
        let imageAddress=cats_population[key].imageAddress;
        //poulateFunction(name,imageAddress);

        if (key<(cats_population.length-1)){
            
        pool.query('INSERT INTO cats (name, imageAddress) VALUES (?, ?)', [name, imageAddress], (error, result)=>{
            if(error){
                return (error);
            }
        });
    }
        else{
            return new Promise((resolve, reject)=>{
                pool.query('INSERT INTO cats (name, imageAddress) VALUES (?, ?)', [name, imageAddress], (error, result)=>{
                if(error){
                    return reject(error);
                }
                return resolve();
            });
        });
        }



    }



    



};
 
module.exports = populate_cats;
