let cats_population = [];
const port = process.env.PORT;
const serverURL = process.env.SERVER_URL;

cats_population=[{"id":1,"name":"kitty cat kitten pet","imageAddress":serverURL+":"+port+"/images/cats/kitty-cat-kitten-pet-45201.jpeg"},{"id":2,"name":"pexels photo 1","imageAddress":serverURL+":"+port+"/images/cats/pexels-photo-1276553.jpeg"},{"id":3,"name":"pexels photo 2","imageAddress":serverURL+":"+port+"/images/cats/pexels-photo-4492150.jpeg"},{"id":4,"name":"pexels photo 3","imageAddress":serverURL+":"+port+"/images/cats/pexels-photo-5427090.jpeg"},{"id":5,"name":"pexels photo 4","imageAddress":serverURL+":"+port+"/images/cats/pexels-photo-7317524.jpeg"}]

module.exports = cats_population;