// populate_users.js
 
const mysql = require('mysql');
 
const pool = mysql.createPool({
    connectionLimit: process.env.CONNECTION_LIMIT,    // the number of connections node.js will hold open to our database
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.MYSQL_DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
});
 
let populate_users = {};

populate_users.insertExampleUsers = (users_population) =>{
    for (const key in users_population) {  
        let email=users_population[key].email
        let name=users_population[key].name;
        let surname=users_population[key].surname;
        let birthDate=users_population[key].birthDate;
        //poulateFunction(name,imageAddress);

        if (key<(users_population.length-1)){
            
            pool.query('INSERT INTO userlist (email, name, surname, birthDate) VALUES (?, ?, ?, ?)', [email, name, surname, birthDate], (error, result)=>{
            if(error){
                return (error);
            }
        });
    }
        else{
            return new Promise((resolve, reject)=>{
                pool.query('INSERT INTO userlist (email, name, surname, birthDate) VALUES (?, ?, ?, ?)', [email, name, surname, birthDate], (error, result)=>{
                if(error){
                    return reject(error);
                }
                return resolve(result.email);
            });
        });
        }



    }



    



};
 
module.exports = populate_users;
