require("dotenv").config();
const express =require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const PORT= process.env.PORT;
const index = require('./routes/index');

const usersRouter = require('./routes/usersRouter');
const catsRouter = require('./routes/catsRouter');
const randomCatRouter = require('./routes/randomCatRouter');
const populateCatsRouter = require('./routes/populateCatsRouter');
const populateUsersRouter = require('./routes/populateUsersRouter');

let path = require('path');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'hbs');

app.use(bodyParser.json());
app.use(cors());
app.use(morgan('dev'));
 
app.use('/api/v1/users',usersRouter);
app.use('/api/v1/cats',catsRouter);
app.use('/cats',randomCatRouter);
app.use('/populate_cats',populateCatsRouter);
app.use('/populate_users',populateUsersRouter);
app.use('/',index);

 
app.listen(PORT, ()=>{
    console.log(`server is listening  on ${PORT}`);
});
 
module.exports = app;
