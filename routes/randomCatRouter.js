//randomCatRouter.js
const request = require("request");
const express =require('express');
const randomCatRouter = express.Router();
const port = process.env.PORT;
const serverURL = process.env.SERVER_URL;
let check_db = require('../db/check_db');

randomCatRouter.get('/', function(req, res) {
    let id = Math.floor((Math.random() * 5) + 1);
    let url = serverURL+':'+port+'/api/v1/cats/'+id;

    // initialize get request
    let options = { method: 'GET',
  url: url,
  headers: 
   { 'cache-control': 'no-cache',
     Connection: 'keep-alive',
     'accept-encoding': 'gzip, deflate',
     Host: serverURL+':'+port,
     'Cache-Control': 'no-cache',
     Accept: '*/*',
     'Content-Type': 'application/json' } };

     // start request
request(options, async function (error, response, body) {

  let catsPopulation = await check_db.getCatsPopulation();
  let catsExamplePopulation = await check_db.getCatsExamplePopulation();

  if (error) throw new Error(error);
  body =  JSON.parse(body);
  if(body.cat.length > 0){
  let catURL = body.cat[0].imageAddress;


  let params = {
    url: catURL,
    catsPopulation: catsPopulation,
    catsExamplePopulation: catsExamplePopulation
 };
 res.render('cats', params);
}
else res.render('cats');
});

  
});


 
module.exports = randomCatRouter;
