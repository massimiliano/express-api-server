//populateUsersRouter.js
 
const express =require('express');
const populateUsersRouter = express.Router();
const users_population = require('../example/users_population');
const populate_users = require('../example/populate_users');
const users_list = require('../db/user_list');
 
populateUsersRouter.post('/',  async (req, res, next)=>{
    try{
 
        const usersPopulation =  await populate_users.insertExampleUsers(users_population).then(()=>{return users_list.getAllUsers();});
        res.json({users: usersPopulation});
 
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
  
module.exports = populateUsersRouter;
