//usersRouter.js
let url = require('url');
const express =require('express');
const usersRouter = express.Router();
const user_list = require('../db/user_list');
 
usersRouter.get('/', async (req, res, next)=>{
    try {
        let q = url.parse(req.url, true);
        let query = q.query;
        if ( typeof query.name !=='undefined'){
            const userByName = await user_list.getUserByName(query);
            res.status(200).json({users: userByName});
            return;

        }
        if ( typeof query.surname !=='undefined'){
            const userBySurname = await user_list.getUserBySurname(query);
            res.status(200).json({users: userBySurname});
            return;
            
        }
        else{
            const users = await user_list.getAllUsers();
            res.status(200).json({users: users});
        }
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
 
usersRouter.param('email', async (req, res, next, email)=> {
    try{
        const user = await user_list.getUserByEmail(email);
        req.user = user;
        next(); // go to usersRouter.get('/:email')
    } catch(e) {
        console.log(e);
        res.sendStatus(404);
    }
});
 
usersRouter.get('/:email',  (req, res, next)=>{
    res.status(200).json({user: req.user});
});
 
 
usersRouter.post('/',  async (req, res, next)=>{
    try{
        const email = req.body.email;
        const name = req.body.name;
        const surname = req.body.surname;
        const birthDate = req.body.birthDate;
         
              if (!email || !name || !surname || !birthDate) {
                return res.sendStatus(400);
             }
 
        const user =  await user_list.insertUser(email, name, surname, birthDate).then(()=>{return user_list.getUserByEmail(email);});
        res.json({user: user});
 
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
 
 
usersRouter.put('/:email',  async (req, res, next)=>{
    try{
        const email = req.body.email;
        const name = req.body.name;
        const surname = req.body.surname;
        const birthDate = req.body.birthDate;
   
              if (!email || !name || !surname || !birthDate) {
                return res.sendStatus(400);
             }
 
        const user =  await user_list.updateUser( name, surname, birthDate, email).then(()=>{return user_list.getUserByEmail(email);});
        res.json({user: user});
         
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
 
 
 
usersRouter.delete('/:email', async (req, res, next)=>{
    try{
        const email = req.params.email;
        const response = await user_list.deleteUser(email);
        return res.sendStatus(204);
 
    } catch(e){
        console.log(e);
    }
})
 
 
 
module.exports = usersRouter;
