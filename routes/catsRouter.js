//catsRouter.js
 
const express =require('express');
const catsRouter = express.Router();
const cats_list = require('../db/cats');
 
catsRouter.get('/', async (req, res, next)=>{
    try {
        const cats = await cats_list.getAllCats();
        res.status(200).json({cats: cats});
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
 
catsRouter.param('catId', async (req, res, next, catId)=> {
    try{
        const cat = await cats_list.getOneCat(catId);
        req.cat = cat;
        next(); // go to catsRouter.get('/:catId')
    } catch(e) {
        console.log(e);
        res.sendStatus(404);
    }
});
 
catsRouter.get('/:catId',  (req, res, next)=>{
    res.status(200).json({cat: req.cat});
});
 
 
catsRouter.post('/',  async (req, res, next)=>{
    try{
        const name = req.body.name;
        const imageAddress = req.body.imageAddress;
         
              if (!name || !imageAddress) {
                return res.sendStatus(400);
             }
 
        const cat =  await cats_list.insertCat(name, imageAddress).then(insertId=>{return cats_list.getOneCat(insertId);});
        res.json({cat: cat});
 
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
 
 
catsRouter.put('/:catId',  async (req, res, next)=>{
    try{
        const name = req.body.name;
        const imageAddress = req.body.imageAddress;
        const catId= req.params.catId;
   
              if (!name || !imageAddress) {
                return res.sendStatus(400);
             }
 
        const cat =  await cats_list.updateCat(name, imageAddress, catId).then(()=>{return cats_list.getOneCat(catId);});
        res.json({cat: cat});
         
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
 
 
 
catsRouter.delete('/:catId', async (req, res, next)=>{
    try{
        const catId = req.params.catId;
        const response = await cats_list.deleteCat(catId);
        return res.sendStatus(204);
 
    } catch(e){
        console.log(e);
    }
})
 
 
 
module.exports = catsRouter;
