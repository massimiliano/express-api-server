// index
const express = require('express');
//const { createPool } = require('mysql');
const check_db = require('../db/check_db');
var indexRouter = express.Router();
const serverURL = process.env.SERVER_URL;
const port = process.env.PORT;

/* GET home page. */

indexRouter.get('/', async function(req, res) {


  let apiDb = await check_db.getApiConnection();
  let userlistConnection = await check_db.getUserlistConnection();
  let userlistPopulation= await check_db.getuserlistPopulation();
  let catsConnection= await check_db.getCatsConnection();
  let catsPopulation= await check_db.getCatsPopulation();

  let params = {
    active: { home: true },
    dbName: process.env.MYSQL_DB,
    connection: apiDb,
    userlistConnection: userlistConnection,
    userlistPopulation: userlistPopulation,
    catsConnection: catsConnection,
    catsPopulation: catsPopulation,
    serverURL: serverURL,
    port: port    
  };
  res.render('index', params);
});

module.exports = indexRouter;
