//populateCatsRouter.js
 
const express =require('express');
const populateCatsRouter = express.Router();
const cats_population = require('../example/cats_population');
const populate_cats = require('../example/populate_cats');
const cats_list = require('../db/cats');
 
populateCatsRouter.post('/',  async (req, res, next)=>{
    try{
 
        const catsPopulation =  await populate_cats.insertExampleCats(cats_population).then(()=>{return cats_list.getAllCats();});
        res.json({cats: catsPopulation});
 
    } catch(e){
        console.log(e);
        res.sendStatus(400);
    }
});
  
module.exports = populateCatsRouter;
