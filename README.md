# Express api server
With this script it is possible to create a (fake)API server connected to a MySQL database.
I added 2 example databases that you can easily deploy and test.
Of course  if you want to connect a new database with different structure, then you need to add/edit some files.
All the content in 'routes', 'db', and of course 'example' are as a working example where some code can be recycled to make it work with your existend Database.
This could be useful for testing or learning purposes i hope.
Feel free to contact me if you want to help improve this code.

To install you need to clone with git clone or download and extract it on you computer/server.

## prerequirements:
NodeJS and MariaDB MySQL

tested with:
 - NodeJS v12.22.7
 - npm v8.1.3
 - MariaDB v15.1


## configuration of MySQL database and tables(for use with examples)

 - **Create a database:**
 `CREATE DATABASE api;`
 - **Change to api database:**
 `use api;`
 - **add two tables:**
  - one named 'userlist':
  `CREATE TABLE userlist (email varchar(255) NOT NULL, name varchar(255) NOT NULL, surname varchar(255) NOT NULL, birthDate date NOT NULL, PRIMARY KEY (email));`
  - one named 'cats':
  `CREATE TABLE cats (id int(11) NOT NULL AUTO_INCREMENT, name varchar(255) NOT NULL, imageAddress varchar(255) NOT NULL, PRIMARY KEY (id));`

## installation of the API server
#### create .env file
use the example.env file (in example folder) and fill the variables for your use case
#### install npm dependencies
npm install

## Start the script
npm start


---
### Once installed visit your host and port with your favourite browser
From there you get further instructions on how to use the API calls and populate the database with the examples
